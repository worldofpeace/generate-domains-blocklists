{
  description = "Utility from dnscrypt-proxy to generate a blocklist for it (worldofpeace's soffftt fork)";

   inputs = {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
      flake-utils.url = "github:numtide/flake-utils";
      flake-compat = {
        url = "github:edolstra/flake-compat";
        flake = false;
      };
    };

  outputs = { self, nixpkgs, flake-utils, ... }:
   flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system}; in
      rec {
        packages.generate-domains-blocklists = pkgs.python3.pkgs.callPackage ./package.nix {};
        defaultPackage = packages.generate-domains-blocklists;
      });
}
