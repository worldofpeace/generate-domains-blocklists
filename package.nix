{ buildPythonApplication
, dnscrypt-proxy2
, lib
}:

buildPythonApplication rec {
  pname = "generate-domains-blocklist";
  version = dnscrypt-proxy2.version; # really a guess

  format = "other";

  src = lib.cleanSource ./.;

  installPhase = ''
    mkdir -p $out/bin
    cp generate-domains-blocklist.py $out/bin/generate-domains-blocklist.py

    # default/example files (I use them)
    mkdir -p $out/share/generate-domains-blocklist
    cp domains-allowlist.txt domains-blocklist.conf \
      domains-blocklist-local-additions.txt domains-time-restricted.txt \
      $out/share/generate-domains-blocklist
  '';

  meta = with lib; {
    inherit (dnscrypt-proxy2.meta) license;
    description = "Utility from dnscrypt-proxy to generate a blocklist for it (worldofpeace's soffftt fork)";
    homepage = "https://gitlab.com/worldofpeace/generate-domains-blocklist";
    maintainers = with maintainers; [ worldofpeace ];
  };
}
